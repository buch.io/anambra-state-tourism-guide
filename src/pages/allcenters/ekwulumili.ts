import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Refresher } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'ekwulumili',
  template:`<ion-header>
  <ion-navbar color="primary">
      <ion-title>
          <ion-icon name="map"></ion-icon> &nbsp; &nbsp; &nbsp; Ekwumili Botanical Garden</ion-title>
  </ion-navbar>
</ion-header>

<ion-content>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7935.796753846561!2d7.098734264137278!3d6.008690277394055!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x10430aa5690333eb%3A0x59e9a77bf159b3f2!2sAguata!5e0!3m2!1sen!2sng!4v1537022704209" width="360" height="360" frameborder="0" style="border:0" allowfullscreen></iframe>
  <ion-list padding>
      <ion-item>
          <ion-icon name="locate"></ion-icon> Aguata LGA,Anambra State.
      </ion-item>
<ion-item>
          <ion-icon name="globe"></ion-icon> http://www.nacd.gov.ng
      </ion-item>
  </ion-list>
  <div *ngFor="let tour of items">
      <h6 padding [innerHTML]="tour.title.rendered"></h6>
      <p padding [innerHTML]="tour.content.rendered"></p>
  </div>
</ion-content>`,
})
export class ekwulumili {

result:any={};
items:any=[];                    
id:any={};
data: Observable<any>;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public loadingCtrl: LoadingController) {
    // If we navigated to this page, we will have an item available as a nav param
  }
  ngOnInit() {
   

    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: `Ekwumili simply Awesome`,
        duration: 3000
    });
loading.present();
    var url = 'http://arinze.myartsonline.com/wp-json/wp/v2/posts/17';
       this.data = this.http.get(url);
       this.data.subscribe(data =>{
    //    console.log(data)
       this.result = data;
    console.log(this.result);
    this.items = [this.result]
    console.log('item', this.items);
    
       
       }, (error)=> {
        console.log(error.error);
        let loading = this.loadingCtrl.create({
          spinner: 'dots',
          content: `Network Error`,
          duration: 3000
        });
        loading.present();
        
      });
     }
  }
