import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Refresher } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'osomari',
  template:`<ion-header>
  <ion-navbar color="primary">
      <ion-title>
          <ion-icon name="map"></ion-icon> &nbsp; &nbsp; &nbsp;OSOMARI FOREST RESERVE</ion-title>
  </ion-navbar>
</ion-header>

<ion-content>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3689.342967531279!2d6.71392770283126!3d5.806666695232518!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1043c85416825911%3A0x3c6b08f6bc39e3cf!2sOsomari+Forest+Reserve!5e1!3m2!1sen!2sng!4v1537183277993" width="360" height="360" frameborder="0" style="border:0" allowfullscreen></iframe>
  <ion-list padding>
      <ion-item>
          <ion-icon name="locate"></ion-icon> RP48+MC Oguaniocho Anambra
      </ion-item>
  </ion-list>
  <div *ngFor="let tour of items">
      <h6 padding [innerHTML]="tour.title.rendered"></h6>
      <p padding [innerHTML]="tour.content.rendered"></p>
  </div>
</ion-content>`,
})
export class osomari {

result:any={};
items:any=[];                    
id:any={};
data: Observable<any>;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public loadingCtrl: LoadingController) {
    // If we navigated to this page, we will have an item available as a nav param
  }
  ngOnInit() {
   

    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: `Osomari`,
        duration: 3000
    });
loading.present();
    var url = 'http://arinze.myartsonline.com/wp-json/wp/v2/posts/42';
       this.data = this.http.get(url);
       this.data.subscribe(data =>{
    //    console.log(data)
       this.result = data;
    console.log(this.result);
    this.items = [this.result]
    console.log('item', this.items);
    
       
       }, (error)=> {
        console.log(error.error);
        let loading = this.loadingCtrl.create({
          spinner: 'dots',
          content: `Network Error`,
          duration: 3000
        });
        loading.present();
        
      });
     }
  }
