import { Component,
  //  ViewChild, ElementRef 
  } from '@angular/core';
// import { Geolocation ,GeolocationOptions ,Geoposition ,PositionError } from '@ionic-native/geolocation';
import { NavController, LoadingController, Refresher } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
// import { touristdetails } from '../home/touristdetail/touristdetails';
import { ogbunike } from '../allcenters/ogbunike';
import { agululake } from '../allcenters/agululake';
import { ikenga } from '../allcenters/ikenga';
import { iyicave } from '../allcenters/iyicave';
import { odoriver } from '../allcenters/odoriver';
import { ekwulumili } from '../allcenters/ekwulumili';
import { iyiocha } from '../allcenters/iyiocha';
import { obutulake } from '../allcenters/obutulake';
import { dikewar } from '../allcenters/dikewar';
import { igboukwumuseum } from '../allcenters/igboukwumuseum';
import { ogbawaterfalls } from '../allcenters/ogbawaterfalls';
import { idemili } from '../allcenters/idemili';
import { rojenny } from '../allcenters/rojenny';
import { odinani } from '../allcenters/odinani';
import { osomari } from '../allcenters/oosomari';
import { ogbajali } from '../allcenters/ogbaajali';
import { historygarden } from '../allcenters/historygraden';
import { okpuana } from '../allcenters/okpuana';
import { oba } from '../allcenters/oba';


// declare var google;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
//   options : GeolocationOptions;
//   currentPos : Geoposition;
//   @ViewChild('map') mapElement: ElementRef;
// map: any;
// places : Array<any> ;

items:any=[];                    
id:any={};
result:any=[];
data: Observable<any>;
  constructor(public navCtrl: NavController, public http: HttpClient, public loadingCtrl: LoadingController, 
    // private geolocation : Geolocation
  ) {
    this.initializeItems();
  }

  ngOnInit() {
   

    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: `Getting Tourist Centers`,
        duration: 3000
    });
// loading.present();
//     var url = 'http://cooufeeds.onlinewebshop.net/wp-json/wp/v2/posts?categories=27&per_page=100&page=1';
//        this.data = this.http.get(url);
//        this.data.subscribe(data =>{
//        console.log(data)
//        this.result = data;
//        this.items = data;
       
//        loading.onDidDismiss(() => {
//         console.log('Dismissed loading');
//       });
       
//        }, (error)=> {
//         console.log(error.error);
//         let loading = this.loadingCtrl.create({
//           spinner: 'dots',
//           content: `Network Error`,
//           duration: 5000
//         });
//         loading.present();
        
//       });
//      }
//      itemTapped(event, Item){
//       this.navCtrl.push(touristdetails,{
//         post: Item
//       });
    }
    refresh(){
      this.ngOnInit()
    }

    initializeItems() {
      this.result = this.items
     }

    getItems(ev: any) {
      // Reset items back to all of the items
      this.initializeItems();
  
      // set val to the value of the searchbar
      const val = ev.target.value;
  
      // if the value is an empty string don't filter the items
      if (val && val.trim() != '') {
        this.result = this.result.filter((post) => {
          return (post.title.rendered.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      }
    }

    Ogbunike() {
      this.navCtrl.push(ogbunike);
    }

    agululake() {
      this.navCtrl.push(agululake);
    }

    ikenga() {
      this.navCtrl.push(ikenga);
    }

    iyicave() {
      this.navCtrl.push(iyicave);
    }

    odoriver() {
      this.navCtrl.push(odoriver);
    }

    obutulake () {
      this.navCtrl.push(obutulake);
    }
    iyiochalake() {
      this.navCtrl.push(iyiocha);
    }
    ekwulumili() {
      this.navCtrl.push(ekwulumili);
    }
    okpuana() {
      this.navCtrl.push(okpuana);
    }

    dikeswar() {
      this.navCtrl.push(dikewar);
    }

    igboukwu() {
      this.navCtrl.push(igboukwumuseum);
    }

    ogbawaterfalls() {
      this.navCtrl.push(ogbawaterfalls);
    }
    idemili() {
      this.navCtrl.push(idemili);
    }
    rojenny() {
      this.navCtrl.push(rojenny);
    }
    oba() {
      this.navCtrl.push(oba);
    }
    odinani() {
      this.navCtrl.push(odinani);
    }
    ogbaajali() {
      this.navCtrl.push(ogbajali);
    }
    osomari() {
      this.navCtrl.push(osomari);
    }
    igboukwugarden() {
      this.navCtrl.push(historygarden);
    }

//   ionViewDidEnter(){
//     this.getUserPosition();
// }
//   getUserPosition(){
//     this.options = {
//         enableHighAccuracy : true
//     };

//     this.geolocation.getCurrentPosition(this.options).then((pos : Geoposition) => {

//         this.currentPos = pos;      
//         console.log(pos);

//     },(err : PositionError)=>{
//         console.log("error : " + err.message);
//     });
// }

// addMap(lat,long){

//   let latLng = new google.maps.LatLng(lat, long);

//   let mapOptions = {
//   center: latLng,
//   zoom: 15,
//   mapTypeId: google.maps.MapTypeId.ROADMAP
//   }

//   this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
//   this.addMarker();

// }

// addMarker(){

//   let marker = new google.maps.Marker({
//   map: this.map,
//   animation: google.maps.Animation.DROP,
//   position: this.map.getCenter()
//   });

//   let content = "<p>This is your current position !</p>";          
//   let infoWindow = new google.maps.InfoWindow({
//   content: content
//   });

//   google.maps.event.addListener(marker, 'click', () => {
//   infoWindow.open(this.map, marker);
//   });

// }

// getRestaurants(latLng)
// {
//     var service = new google.maps.places.PlacesService(this.map);
//     let request = {
//         location : latLng,
//         radius : 8047 ,
//         types: ["restaurant"]
//     };
//     return new Promise((resolve,reject)=>{
//         service.nearbySearch(request,function(results,status){
//             if(status === google.maps.places.PlacesServiceStatus.OK)
//             {
//                 resolve(results);    
//             }else
//             {
//                 reject(status);
//             }

//         }); 
//     });

// }

// createMarker(place)
// {
//     let marker = new google.maps.Marker({
//     map: this.map,
//     animation: google.maps.Animation.DROP,
//     position: place.geometry.location
//     });   
// }

// addMap2(lat,long){

//   let latLng = new google.maps.LatLng(lat, long);

//   let mapOptions = {
//   center: latLng,
//   zoom: 15,
//   mapTypeId: google.maps.MapTypeId.ROADMAP
//   }

//   this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

//   this.getRestaurants(latLng).then((results : Array<any>)=>{
//       this.places = results;
//       for(let i = 0 ;i < results.length ; i++)
//       {
//           this.createMarker(results[i]);
//       }
//   },(status)=>console.log(status));

//   this.addMarker();

// }

}
