import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';
import { Geolocation,
  // GeolocationOptions ,Geoposition ,PositionError
 } from '@ionic-native/geolocation';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { touristdetails } from '../pages/home/touristdetail/touristdetails';
import { ogbunike } from '../pages/allcenters/ogbunike';
import { agululake } from '../pages/allcenters/agululake';
import { ikenga } from '../pages/allcenters/ikenga';
import { iyicave } from '../pages/allcenters/iyicave';
import { odoriver } from '../pages/allcenters/odoriver';
import { obutulake } from '../pages/allcenters/obutulake';
import { iyiocha } from '../pages/allcenters/iyiocha';
import { ekwulumili } from '../pages/allcenters/ekwulumili';
import { okpuana } from '../pages/allcenters/okpuana';
import { dikewar } from '../pages/allcenters/dikewar';
import { igboukwumuseum } from '../pages/allcenters/igboukwumuseum';
import { ogbawaterfalls } from '../pages/allcenters/ogbawaterfalls';
import { idemili } from '../pages/allcenters/idemili';
import { rojenny } from '../pages/allcenters/rojenny';
import { oba } from '../pages/allcenters/oba';
import { odinani } from '../pages/allcenters/odinani';
import { ogbajali } from '../pages/allcenters/ogbaajali';
import { osomari } from '../pages/allcenters/oosomari';
import { historygarden } from '../pages/allcenters/historygraden';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    touristdetails,
    ogbunike,
    agululake,
    ikenga,
    iyicave,
    odoriver,
    obutulake,
    iyiocha,
    ekwulumili,
    okpuana,
    dikewar,
    igboukwumuseum,
    ogbawaterfalls,
    idemili,
    rojenny,
    oba,
    odinani,
    ogbajali,
    osomari,
    historygarden,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    touristdetails,
    ogbunike,
    agululake,
    ikenga,
    iyicave,
    odoriver,
    obutulake,
    iyiocha,
    ekwulumili,
    okpuana,
    dikewar,
    igboukwumuseum,
    ogbawaterfalls,
    idemili,
    rojenny,
    oba,
    odinani,
    ogbajali,
    osomari,
    historygarden,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
